'use strict';

const config = {
  twitter: {
    token: {
      consumer_key:         ( process.env.NYBOT_TWITTER_CONSUMER_KEY        || '' ),
      consumer_secret:      ( process.env.NYBOT_TWITTER_CONSUMER_SECRET     || '' ),
      access_token:         ( process.env.NYBOT_TWITTER_ACCESS_TOKEN        || '' ),
      access_token_secret:  ( process.env.NYBOT_TWITTER_ACCESS_TOKEN_SECRET || '' )
    },
    name: ( process.env.NYBOT_TWITTER_SCREEN_NAME || '' ),
  },
  slack: {
    token:  ( process.env.NYBOT_SLACK_TOKEN         || '' ),

    channel: {
      name:       ( process.env.NYBOT_SLACK_CHANNEL || '' ),
      isPrivate:  ( ( process.env.NYBOT_SLACK_CHANNEL_PRIVATE !== '' ) ? true : false )
    }
  },

  logger: {
    level: ( process.env.NYBOT_LOG_LEVEL || 'info' ),
  }
};

if ( config.twitter.token.consumer_key === '' ) {
  throw new Error('consumer_key for twitter is not specified.');
}

if ( config.twitter.token.consumer_secret === '' ) {
  throw new Error('consumer_secret for twitter is not specified.');
}

if ( config.twitter.token.access_token === '' ) {
  throw new Error('access_token for twitter is not specified.');
}

if ( config.twitter.token.access_token_secret === '' ) {
  throw new Error('access_token_secret for twitter is not specified.');
}

if ( config.twitter.name === '' ) {
  throw new Error('screen name on twitter is not specified.');
}

if ( config.slack.token === '' ) {
  throw new Error('slack access token is not specified.');
}

if ( config.slack.channel.name === '' ) {
  throw new Error('channel name of slack is not specified.');
}

if ( ! config.slack.channel.isPrivate ) {
  throw new Error('public channel on slack is not supported yet.');
}

module.exports = config;
