'use strict';

const config    = require('./config').logger;
const Log       = require('log');

module.exports  = new Log(config.level);

