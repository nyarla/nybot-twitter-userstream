'use strict';

const config    = require('./config').slack;
const message   = require('./message');
const logger    = require('./logger');

const Promise   = require('bluebird');
const Entities  = require('html-entities').AllHtmlEntities;
const slack     = require('@slack/client');

const RtmClient = slack.RtmClient;
const WebClient = slack.WebClient;

const EVENTS    = slack.RTM_EVENTS;

let _slackChannelId = null;

const client    = new WebClient(config.token);
const stream    = new RtmClient(config.token);
const decoder   = new Entities();

function getPrivateChannelIdByName(name) {
  if ( _slackChannelId === null ) {
    return client.groups.list({}).then((data) => {
      for ( let idx = 0, len = data.groups.length; idx < len; idx++ ) {
        const group = data.groups[idx];

        if ( group.name === name ) {
          _slackChannelId = group.id;

          return Promise.resolve(_slackChannelId);
        }
      }
      
      return Promise.reject(new Error('could not found the private group id by this name: ' + name));
    });
  }

  return Promise.resolve(_slackChannelId);
}

function post(payload) {
  if ( config.channel.isPrivate ) {
    return getPrivateChannelIdByName(config.channel.name).then((channel) => {
      const message = decoder.decode(payload.message);
      const options = payload.options;

      return client.chat.postMessage(channel, message, options);
    });
  }
}

function listen(tweet) {
  stream.start();
  stream.on(EVENTS.MESSAGE, (msg) => {
    if ( typeof(msg.bot_id) === 'undefined' && typeof(msg.user) !== 'undefined' ) {
      return tweet(msg.text).catch((err) => {
        if ( err.toString.match(/This message is over than 140 chars/)) {
          const name    = 'ERROR';
          const icon    = ':bangbang:';
          const text    = err.toString();
          const payload = {
            message: text,
            options: message.createOptions(name, icon)
          };

          return post(payload);
        }
      });
    }
  });
}

exports.post    = post    ;
exports.listen  = listen  ;
