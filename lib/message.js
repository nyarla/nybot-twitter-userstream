'use strict';

const TYPES = {
  TL:     0,
  DM:     1,
  RT:     2,
  FAV:    3,
  FOLLOW: 4
};

exports.TYPES = TYPES;

exports.createSlackMessage = function createSlackMessage(type, text) {
  switch ( type ) {
    case TYPES.DM:
      text = ':email: ' + text;
    break;

    case TYPES.RT:
      text = ':recycle: ' + text;
    break;

    case TYPES.FAV:
      text = ':star: ' + text;
    break;
  }

  return text;
};

exports.createSlackOptions = function createSlackOptions(uname, icon) {
  let options = {
    unfurl_links: true,
    unfurl_media: true,
    
    as_user:  false,
    username: uname
  };

  if ( icon.match(/^http/) ) {
    options.icon_url = icon;
  }

  if ( icon.match(/^:[^:]+:$/) ) {
    options.icon_emoji = icon;
  }

  return options;
};


