'use strict';

const config    = require('./config');
const message   = require('./message');
const logger    = require('./logger');

const Promise   = require('bluebird');
const Entities  = require('html-entities').AllHtmlEntities;
const Text      = require('twitter-text');
const twit      = require('promised-twit');

const client  = new twit(config.twitter.token);
const stream  = client.stream('user');
const decoder = new Entities();

function listen(post) {
  stream.on('tweet', (tweet) => {
    const name    = tweet.user.screen_name;

    if ( name === config.twitter.name ) {
      return
    }

    const text    = tweet.text;
    const icon    = tweet.user.profile_image_url;
    const id      = tweet.id;

    const payload = {
      message: message.createSlackMessage(message.TYPES.TL, text),
      options: message.createSlackOptions(name, icon)
    };

    post(payload).catch((err) => {
      logger.error(err.toString());
    });
  });

  stream.on('direct_message', (msg) => {
    const name    = msg.sender.screen_name;
    const text    = '@' + config.slack.name + ' ' + message.text;
    const icon    = msg.sender.profile_image_url;

    const payload = {
      message: message.createSlackMessage(message.TYPES.DM, text),
      options: message.createSlackOptions(name, icon)
    };

    post(payload).catch((err) => {
      logger.error(err.toString());
    });
  });

  stream.on('user_event', (event) => {
    const name = event.source.name;
    const icon = event.source.profile_image_url;

    let type = null;
    let text = null;

    switch (event.event) {
      case 'favorite':
        type = message.TYPES.FAV;
        text = event.target_object.text;
      break;

      case 'follow':
        type = message.TYPES.FOLLOW;
        text = '@' + name + 'is followed to you.';
      break;
    }

    if ( type !== null && text !== null ) {
      const payload = {
        message: message.createSlackMessage(type, text),
        options: message.createSlackOptions(name, icon)
      };

      post(payload).catch((err) => {
        logger.error(err.toString());
      });
    }
  });
}

function tweet(text) {
  text = decoder.decode(text);

  const len = Text.getTweetLength(text);

  if ( len > 140 ) {
    return Promise.reject(new Error('This message is over than 140 chars: ' + len));
  }

  return client.postStatusesUpdate({ status: text });
}

exports.listen    = listen    ;
exports.tweet     = tweet     ;
