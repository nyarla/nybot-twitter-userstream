'use strict';

const M = require('../lib/message');
const assert = require('assert');

describe('lib/message', () => {
  context('#createSlackMessage', () => {
    it('should returns DM message', () => {
      assert.equal(M.createSlackMessage(M.TYPES.DM, 'hello'), ':email: hello');
    });

    it('should returns RT message', () => {
      assert.equal(M.createSlackMessage(M.TYPES.RT, 'hello'), ':recycle: hello');
    });

    it('should returns FAV message', () => {
      assert.equal(M.createSlackMessage(M.TYPES.FAV, 'hello'), ':star: hello');
    });

    it('should returns non decorated message', () => {
      assert.equal(M.createSlackMessage(M.TYPES.TL, 'hello'), 'hello');
      assert.equal(M.createSlackMessage(M.TYPES.FOLLOW, 'hello'), 'hello');
    });
  });

  context('#createSlackOptions', () => {
    it('should returns object for slack post message options', () => {
      assert.deepEqual(
        M.createSlackOptions('nyarla', 'http://example.com/icon.jpg'),
        {
          unfurl_links: true,
          unfurl_media: true,
          as_user: false,
          username: 'nyarla',
          icon_url: 'http://example.com/icon.jpg'
        }
      );

      assert.deepEqual(
        M.createSlackOptions('kalaclism', ':recycle:', 1),
        {
          unfurl_links: true,
          unfurl_media: true,
          as_user: false,
          username: 'kalaclism',
          icon_emoji: ':recycle:'
        }
      );
    });
  });
});
