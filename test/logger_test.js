'use strict';

const assert = require('assert');

describe('lib/logger', () => {
  context('on require', () => {
    it('should be loadable', () => {
      process.env.NYBOT_TWITTER_CONSUMER_KEY = 'x';
      process.env.NYBOT_TWITTER_CONSUMER_SECRET = 'x';
      process.env.NYBOT_TWITTER_ACCESS_TOKEN = 'x';
      process.env.NYBOT_TWITTER_ACCESS_TOKEN_SECRET = 'x';
      process.env.NYBOT_TWITTER_SCREEN_NAME = 'x';
      process.env.NYBOT_SLACK_TOKEN = 'x';
      process.env.NTBOT_SLACK_VERIFY_TOKEN = 'x';
      process.env.NYBOT_SLACK_NAME = 'x';
      process.env.NYBOT_SLACK_CHANNEL = 'x';
      process.env.NYBOT_SLACK_CHANNEL_PRIVATE = 'yes';
      process.env.NYBOT_LOG_LEVEL = 'debug';

      assert.ok( require('../lib/logger') );
    });
  });
});
