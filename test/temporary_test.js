'use strict';

const assert = require('assert');

describe('lib/twitter', () => {
  context('on require', () => {
    it('should be loadable', () => {
      assert.ok(require('../lib/twitter'));
    })
  })
});

describe('lib/slack', () => {
  context('on require', () => {
    it('should be loadable', () => {
      assert.ok(require('../lib/slack'));
    })
  })
});
