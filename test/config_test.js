'use strict';

const assert = require('assert');

describe('lib/config', () => {
  context('on require', () => {
    it('should be loadable', () => {
      process.env.NYBOT_TWITTER_CONSUMER_KEY = 'x';
      process.env.NYBOT_TWITTER_CONSUMER_SECRET = 'x';
      process.env.NYBOT_TWITTER_ACCESS_TOKEN = 'x';
      process.env.NYBOT_TWITTER_ACCESS_TOKEN_SECRET = 'x';
      process.env.NYBOT_TWITTER_SCREEN_NAME = 'x';
      process.env.NYBOT_SLACK_TOKEN = 'x';
      process.env.NYBOT_SLACK_VERIFY_TOKEN = 'x';
      process.env.NYBOT_SLACK_NAME = 'x';
      process.env.NYBOT_SLACK_CHANNEL = 'x';
      process.env.NYBOT_SLACK_CHANNEL_PRIVATE = 'yes';
      process.env.NYBOT_LOG_LEVEL = 'debug';

      const config = require('../lib/config');

      assert.deepEqual(config, {
        twitter: {
          token: {
            consumer_key: 'x',
            consumer_secret: 'x',
            access_token: 'x',
            access_token_secret: 'x'
          },
          name: 'x'
        },
        slack: {
          token: 'x',
          channel: {
            name: 'x',
            isPrivate: true
          }
        },

        logger: {
          level: 'debug'
        }
      });
    });
  });
});
