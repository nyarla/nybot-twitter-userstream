'use strict';

const slack   = require('./lib/slack');
const twitter = require('./lib/twitter');

twitter.listen(slack.post);
slack.listen(twitter.tweet);

const handler = (r, w) => {
  w.setHeader('Content-Type', 'text/plain');
  w.end('NyBOT is working!');
};

require('http').createServer(handler).listen(process.env.PORT || 3000);
