nybot-twitter-userstream
========================

A Slack bot for posting twitter userstream to slack private channel.

this bot is testing and running on dokku 0.6.2 on digitalocean.

USAGE
-----

```bash
$ git clone https://github.com/nyarla/nybot-twitter-userstream.git
$ cd nybot-twitter-userstream
$ git remote add dokku dokku@dokku.me:nybot-t2s
$ git push dokku master
```

AUTHOR
------

Naoki OKAMURA (Nyarla) <nyarla@thotep.net>

LICENSE
-------

MIT

